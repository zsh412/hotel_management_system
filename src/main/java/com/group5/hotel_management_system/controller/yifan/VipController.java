package com.group5.hotel_management_system.controller.yifan;
import com.group5.hotel_management_system.service.yifan.VipInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@RestController
public class VipController {

    @Resource
    private VipInfoService vipInfoService;
    /**
     * 通过RequestMapping设置该方法，请求的路径
     * @return
     */

    @RequestMapping("/vip_all")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=vipInfoService.findAll();
        return list;
    }

    @RequestMapping("/vip_save")
    public String save(@RequestParam Map<String,String> map){
        String msg="添加失败";
        boolean flag=vipInfoService.doSave(map);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }
    @RequestMapping("/vip_delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg="删除失败";
        boolean flag=vipInfoService.doDelete(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }
    @RequestMapping("/vip_edit")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=vipInfoService.doUpdate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }
    @RequestMapping("/vip_findbyid")
    public Map<String,Object> findById(@RequestParam Map<String,String> map){
        Map<String, Object> res=vipInfoService.findById(map);
        return res;
    }
    @RequestMapping("/vip_findbyname")
    public Map<String,Object> findByName(@RequestParam Map<String,String> map){
        Map<String, Object> res=vipInfoService.findByName(map);
        return res;
    }
    @RequestMapping("/vip_findbyIdCard")
    public Map<String,Object> findByIdCard(@RequestParam Map<String,String> map){
        Map<String, Object> res=vipInfoService.findByIdCard(map);
        return res;
    }
}
