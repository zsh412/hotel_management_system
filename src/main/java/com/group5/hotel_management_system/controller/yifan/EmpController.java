package com.group5.hotel_management_system.controller.yifan;
import com.group5.hotel_management_system.service.yifan.EmpInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@RestController
public class EmpController {
    @Resource
    private EmpInfoService empInfoService;

    @RequestMapping("/emp_all")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=empInfoService.findAll();
        if(list == null)
            System.out.println("list is null");
        return list;
    }

    @RequestMapping("/emp_save")
    public String save(@RequestParam Map<String,String> map){
        String msg="添加失败";
        boolean flag=empInfoService.doSave(map);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }
    @RequestMapping("/emp_delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg="删除失败";
        boolean flag=empInfoService.doDelete(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }
    @RequestMapping("/emp_edit")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=empInfoService.doUpdate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }
    @RequestMapping("/emp_findbyid")
    public Map<String,Object> findById(@RequestParam Map<String,String> map){
        Map<String, Object> res=empInfoService.findById(map);
        return res;
    }
    @RequestMapping("/emp_findbyname")
    public Map<String,Object> findByName(@RequestParam Map<String,String> map){
        Map<String, Object> res=empInfoService.findByName(map);
        return res;
    }
    @RequestMapping("/emp_findbyidcard")
    public Map<String,Object> findByIdCard(@RequestParam Map<String,String> map){
        Map<String, Object> res=empInfoService.findByIdCard(map);
        return res;
    }
}
