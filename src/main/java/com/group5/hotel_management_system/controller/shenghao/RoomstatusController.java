package com.group5.hotel_management_system.controller.shenghao;

import com.group5.hotel_management_system.service.shenghao.RoomService;
import com.group5.hotel_management_system.service.shenghao.RoomstatusService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RoomstatusController {
    @Resource
    RoomstatusService roomstatusService;

    /**
     * RequestParam 就是通知服务器，前端提交的数据封装成Map集合
     *          以表单控件的name属性的值作为map集合的key
     *          以表单控件的value属性的值作为map集合的value
     * @param map
     * @return
     */
    @RequestMapping("/roomstatus_save")
    public String save(@RequestParam Map<String,String> map){
        String msg="添加失败";
        boolean flag=roomstatusService.doSave(map);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }

    @RequestMapping("/roomstatus_create")
    public String create(@RequestParam String tableName){
        String msg="添加失败";
        tableName= "t_"+tableName+"_info";
        boolean flag=roomstatusService.doCreate(tableName);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }

    @RequestMapping("/roomstatus_delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg="删除失败";
        boolean flag=roomstatusService.doDelete(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }
    @RequestMapping("/roomstatus_edit")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=roomstatusService.doUpdate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }

    @RequestMapping("/roomstatus_editbydate")
    public String updateByDate(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=roomstatusService.doUpdateByDate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }
    @RequestMapping("/roomstatus_findbyid")
    public Map<String,Object> findById(@RequestParam Map<String,String> map){
        Map<String, Object> res=roomstatusService.findById(map);
        return res;
    }

    @RequestMapping("/roomstatus_findbydate")
    public Map<String,Object> findByDate(@RequestParam Map<String,String> map){
        Map<String, Object> res=roomstatusService.findByDate(map);

        //因为jQuary.post如果获取的是空的话，他下面的函数都不会执行，所以赋一个值，不让他为空
        if(res==null){
            res=new HashMap<>();
            res.put("roomStatus","空闲");
        }
        return res;
    }
    @RequestMapping("/roomstatus_all")
    public List<Map<String,Object>> findAll(@RequestParam Map<String,String> map){
        List<Map<String,Object>> list=roomstatusService.findAll(map);
        return list;
    }
}
