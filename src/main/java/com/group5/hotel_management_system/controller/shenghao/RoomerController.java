package com.group5.hotel_management_system.controller.shenghao;

import com.group5.hotel_management_system.mapper.shenghao.RoomerInfoMapper;
import com.group5.hotel_management_system.service.shenghao.RoomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class RoomerController {
    @Resource
    RoomerService roomerService;

    /**
     * RequestParam 就是通知服务器，前端提交的数据封装成Map集合
     *          以表单控件的name属性的值作为map集合的key
     *          以表单控件的value属性的值作为map集合的value
     * @param map
     * @return
     */
    @RequestMapping("/roomer_save")
    public String save(@RequestParam Map<String,String> map){
        String msg="信息填写失败";
        boolean flag=roomerService.doSave(map);
        if(flag){
            msg="信息填写成功";
        }
        return msg;
    }


    @RequestMapping("/roomer_delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg="删除失败";
        boolean flag=roomerService.doDelete(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }
    @RequestMapping("/roomer_edit")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=roomerService.doUpdate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }
    @RequestMapping("/roomer_findbyid")
    public Map<String,Object> findById(@RequestParam Map<String,String> map){
        Map<String, Object> res=roomerService.findById(map);
        return res;
    }
    @RequestMapping("/roomer_findByIdCard")
    public Map<String,Object> findByIdCard(@RequestParam Map<String,String> map){
        Map<String, Object> res=roomerService.findByIdCard(map);
        return res;
    }

    @RequestMapping("/roomer_all")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=roomerService.findAll();
        return list;
    }
}
