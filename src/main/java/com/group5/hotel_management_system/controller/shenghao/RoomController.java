package com.group5.hotel_management_system.controller.shenghao;

import com.group5.hotel_management_system.service.shenghao.RoomService;
import com.group5.hotel_management_system.service.shenghao.RoomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class RoomController {
    @Resource
    RoomService roomService;

    /**
     * RequestParam 就是通知服务器，前端提交的数据封装成Map集合
     *          以表单控件的name属性的值作为map集合的key
     *          以表单控件的value属性的值作为map集合的value
     * @param map
     * @return
     */
    @RequestMapping("/room_save")
    public String save(@RequestParam Map<String,String> map){
        String msg="添加失败";
        boolean flag=roomService.doSave(map);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }


    @RequestMapping("/room_delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg="删除失败";
        boolean flag=roomService.doDelete(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }
    @RequestMapping("/room_edit")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=roomService.doUpdate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }

    @RequestMapping("/room_findbyid")
    public Map<String,Object> findById(@RequestParam Map<String,String> map){
        Map<String, Object> res=roomService.findById(map);
        return res;
    }
    @RequestMapping("/room_findbynum")
    public Map<String,Object> findByNum(@RequestParam Map<String,String> map){
        Map<String, Object> res=roomService.findByNum(map);
        return res;
    }
    @RequestMapping("/room_all")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=roomService.findAll();
        return list;
    }
}
