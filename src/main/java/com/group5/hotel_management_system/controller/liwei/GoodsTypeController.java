package com.group5.hotel_management_system.controller.liwei;

import com.group5.hotel_management_system.service.liwei.GoodsInfoService;

import com.group5.hotel_management_system.service.liwei.GoodsTypeService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GoodsTypeController {
    @Resource
    private GoodsTypeService goodsTypeService;

    @RequestMapping("/goodsType_all")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list = goodsTypeService.findAllGoodsType();
        return list;
    }

    @RequestMapping("/goodsType_save")
    public String save(@RequestParam Map<String,String> map){
        String msg="添加失败";
        boolean flag=goodsTypeService.doSaveGoodsType(map);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }

    @RequestMapping("/goodsType_delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg="删除失败";
        boolean flag=goodsTypeService.doDeleteGoodsType(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }

    @RequestMapping("/goodsType_edit")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=goodsTypeService.doUpdateGoodsType(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }

    @RequestMapping("/goodsType_findById")
    public Map<String,Object> findById(@RequestParam Map<String,String> map){
        Map<String, Object> res = goodsTypeService.findByIdGoodsType(map);

        //如果按照编号没有查到东西，返回未查到某物
        if(res==null){
            res = new HashMap<>();
            res.put("goodsType_name","未查到");
        }
        return res;
    }

    @RequestMapping("/goodsType_findByName")
    public Map<String,Object> findByName(@RequestParam Map<String,String> map){
        Map<String, Object> res = goodsTypeService.findByNameGoodsType(map);

        //如果按名称没有查到东西，返回未查到某物
        if(res==null){
            res = new HashMap<>();
            res.put("goodsType_name","未查到");
        }
        return res;
    }
}