package com.group5.hotel_management_system.controller.haoran;

import com.group5.hotel_management_system.service.haoran.BuyInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BuyInfoController {
    @Resource
    BuyInfoService buyInfoService;
    /**
     * RequestParam 就是通知服务器，前端提交的数据封装成Map集合
     *          以表单控件的name属性的值作为map集合的key
     *          以表单控件的value属性的值作为map集合的value
     * @param map
     * @return
     */
    @RequestMapping("/buy_save")
    public String save(@RequestParam Map<String,String> map){
        String msg="添加失败";
        boolean flag=buyInfoService.doSave(map);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }

    @RequestMapping("/roomBuy_create")
    public String create(@RequestParam String tableName){
        String msg="添加失败";
        tableName= "t_"+tableName+"buy_info";
        boolean flag=buyInfoService.doCreate(tableName);
        if(flag){
            msg="添加成功";
        }
        return msg;
    }

    @RequestMapping("/buy_delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg="删除失败";
        boolean flag=buyInfoService.doDelete(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }
    @RequestMapping("/buy_edit")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=buyInfoService.doUpdate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }
    @RequestMapping("/buy_findbyid")
    public Map<String,Object> findById(@RequestParam Map<String,String> map){
        Map<String, Object> res=buyInfoService.findById(map);
        return res;
    }

    @RequestMapping("/buy_all")
    public List<Map<String,Object>> findAll(@RequestParam Map<String,String> map){
        List<Map<String,Object>> list=buyInfoService.findAll(map);
        if(list.isEmpty()){
            //因为返回值为空时，在html里无法通过返回值为null判断是否为空，所以给他赋一个值
            //检测返回值  list[0].id=="未找到"
            Map xxx=new HashMap();
            xxx.put("id","未找到");
            list=new ArrayList<>();
            list.add(xxx);
        }
        return list;
    }
    @RequestMapping("/buy_findByName")
    public Map<String,Object> findByName(@RequestParam Map<String,String> map){
        Map<String, Object> res=buyInfoService.findByName(map);

        //因为jQuary.post如果获取的是空的话，他下面的函数都不会执行，所以赋一个值，不让他为空
        if(res==null){
            res=new HashMap<>();
            res.put("goods","空");
        }
        return res;
    }
    //清空数据库
    @RequestMapping("/buy_clear")
    public void clear(@RequestParam Map<String,String> map){
        buyInfoService.clear(map);
    }


}
