package com.group5.hotel_management_system.controller.zhujiong;

import com.group5.hotel_management_system.service.zhujiong.LogInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class LogInfoController {
    @Resource
    private LogInfoService logInfoService;

    @RequestMapping("/all_log")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list= logInfoService.findAll();
        return list;
    }

    @RequestMapping("/saveLog")
    public String save(@RequestParam Map<String,String> map) {
        String msg="新建失败";
        boolean flag=logInfoService.saveLog(map);
        if(flag){
            msg="新建成功";
        }
        return msg;
    }
}
