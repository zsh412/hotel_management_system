package com.group5.hotel_management_system.controller.zhujiong;

import com.group5.hotel_management_system.service.zhujiong.RoomListService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class RoomListController {
    @Resource
    private RoomListService roomListService;

    @RequestMapping("/room_list")
    public List<Map<String,Object>> findAllRoom(){
        List<Map<String,Object>> list= roomListService.findAllRoom();
        return list;
    }

    @RequestMapping("/edit_room")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag= roomListService.updateRoom(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }

    @RequestMapping("/deleteRoom")
    public String deleteRoom(@RequestParam Map<String,String> map){
        String msg = "删除失败";
        boolean flag = roomListService.deleteRoom(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }

    @RequestMapping("/newRoom")
    public String save(@RequestParam Map<String,String> map) {
        String msg="新建失败";
        boolean flag=roomListService.saveRoom(map);
        if(flag){
            msg="新建成功";
        }
        return msg;
    }

    @RequestMapping("/edit_type")
    public String updateType(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag= roomListService.updateType(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }

}
