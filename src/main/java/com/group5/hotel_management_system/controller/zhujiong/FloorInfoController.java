package com.group5.hotel_management_system.controller.zhujiong;

import java.util.List;
import java.util.Map;

import com.group5.hotel_management_system.service.zhujiong.FloorInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

@RestController
public class FloorInfoController {

    @Resource
    private FloorInfoService floorInfoService;

    @RequestMapping("/all_floor")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list= floorInfoService.findAll();
        return list;
    }

    @RequestMapping("/edit_floor")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=floorInfoService.doUpdate(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam Map<String,String> map){
        String msg = "删除失败";
        boolean flag = floorInfoService.doDelete(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }

    @RequestMapping("/newFloor")
    public String save(@RequestParam Map<String,String> map) {
        String msg="新建失败";
        boolean flag=floorInfoService.saveFloor(map);
        if(flag){
            msg="新建成功";
        }
        return msg;
    }
}
