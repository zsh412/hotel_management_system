package com.group5.hotel_management_system.controller.zhujiong;

import com.group5.hotel_management_system.service.zhujiong.RoomTypeInfoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class RoomTypeInfoController {
    @Resource
    private RoomTypeInfoService roomTypeInfoService;

    @RequestMapping("/all_roomType")
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list= roomTypeInfoService.findAllRoomType();
        return list;
    }

    @RequestMapping("/edit_roomType")
    public String update(@RequestParam Map<String,String> map){
        String msg="修改失败";
        boolean flag=roomTypeInfoService.doUpdateRoomType(map);
        if(flag){
            msg="修改成功";
        }
        return msg;
    }

    @RequestMapping("/delete_roomType")
    public String delete(@RequestParam Map<String,String> map){
        String msg = "删除失败";
        boolean flag = roomTypeInfoService.doDeleteRoomType(map);
        if(flag){
            msg="删除成功";
        }
        return msg;
    }

    @RequestMapping("/newRoomType")
    public String save(@RequestParam Map<String,String> map) {
        String msg="新建失败";
        boolean flag=roomTypeInfoService.saveRoomType(map);
        if(flag){
            msg="新建成功";
        }
        return msg;
    }

    @RequestMapping("/roomtype_findbytype")
    public Map<String,Object> findByType(@RequestParam Map<String,String> map) {
        Map<String,Object> type=roomTypeInfoService.findByType(map);
        return type;
    }
}
