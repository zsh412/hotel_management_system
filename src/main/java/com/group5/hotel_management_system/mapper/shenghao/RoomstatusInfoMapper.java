package com.group5.hotel_management_system.mapper.shenghao;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 这个mapper是用来创建不同房间对不同的表格
 * 例如：我们新创建了房间A101，在这里我们动态创建 "t_A101_info" 表格
 * 因为我们需要操作的表格不同，所以函数参数中会加一个tableName
 */
@Mapper
public interface RoomstatusInfoMapper {

    //尝试去创建表，创建成功，但是返回值为0
    @Update("CREATE TABLE  IF NOT EXISTS  ${tableName}  (\n" +
            "  `id` int(0) NOT NULL AUTO_INCREMENT,\n" +
            "  `roomStatus` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '房间状态',\n" +
            "  `time_in` date NOT NULL COMMENT '(预计)入住时间:2020.6.13',\n" +
            "  `time_out` date NULL DEFAULT NULL COMMENT '预计离开时间:2020.6.14',\n" +
            "  `roomer_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '房客身份证id',\n" +
            "  `deposit` int(0) NULL DEFAULT NULL COMMENT '押金：250',\n" +
            "  PRIMARY KEY (`id`) USING BTREE\n" +
            ") ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;")
    public  int create(String tableName);

    /**
     * 将来数据来自页面，来自页面的数据都是字符串
     * @Param("room") 表示参数的别名，在sql中使用
     * 参数分别是状态、入住时间、离开时间、房客的身份证id,押金
     */
    @Insert("insert into ${room.tableName}(roomStatus,time_in,time_out,roomer_id,deposit)"
            +"value(#{room.roomStatus},#{room.time_in},#{room.time_out},#{room.roomer_id},#{room.deposit})")
    public int save(@Param("room") Map<String,String> map);


    @Update("update ${room.tableName} " +
            " set roomStatus=#{room.roomStatus},time_in=#{room.time_in},time_out=#{room.time_out}," +
            "roomer_id=#{room.roomer_id},deposit=#{room.deposit}" +
            " where id=#{room.id}")
    public int update(@Param("room") Map<String,String> map);

    //修改入住日期为room.time_in的信息
    @Update("update ${room.tableName} " +
            " set roomStatus=#{room.roomStatus},time_out=#{room.time_out}," +
            "roomer_id=#{room.roomer_id},deposit=#{room.deposit}" +
            " where time_in=#{room.time_in}")
    public int updateByDate(@Param("room") Map<String,String> map);

    @Delete("delete from ${room.tableName} where id=#{room.id}")
    public int delete(@Param("room") Map<String,String> map);

    @Select("select * from ${room.tableName} where id=#{room.id}")
    public Map<String,Object> findById(@Param("room") Map<String,String> map);

    @Select("select * from ${room.tableName} where time_in=#{room.time_in}")
    public Map<String,Object> findByDate(@Param("room") Map<String,String> map);

    @Select("select * from ${room.tableName}")
    public List<Map<String,Object>> findAll(@Param("room") Map<String,String> map);

}
