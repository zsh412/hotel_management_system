package com.group5.hotel_management_system.mapper.shenghao;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoomInfoMapper {
    /**
     * 将来数据来自页面，来自页面的数据都是字符串
     * @Param("room") 表示参数的别名，在sql中使用
     */
    @Insert("insert into t_room_info(room_num,room_type,room_floor)"
            +"value(#{room.num},#{room.type},#{room.floor})")
    public int save(@Param("room") Map<String,String> map);

    @Update("update t_room_info " +
            " set room_num=#{room.num},room_type=#{room.type},room_floor=#{room.floor}" +
            " where id=#{room.id}")
    public int update(@Param("room") Map<String,String> map);

    @Delete("delete from t_room_info where id=#{id}")
    public int delete(int id);

    @Select("select * from t_room_info where id=#{id}")
    public Map<String,Object> findById(int id);

    @Select("select * from t_room_info where room_num=#{num}")
    public Map<String,Object> findByNum(String num);

    @Select("select * from t_room_info")
    public List<Map<String,Object>> findAll();

}
