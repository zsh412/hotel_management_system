package com.group5.hotel_management_system.mapper.shenghao;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoomerInfoMapper {

    /**
     * 将来数据来自页面，来自页面的数据都是字符串
     * @Param("roomer") 表示参数的别名，在sql中使用
     */
    @Insert("insert into t_roomer_info(roomer_name,roomer_tel,roomer_sex,roomer_idnumber,roomer_isvip)"
            +"value(#{roomer.name},#{roomer.tel},#{roomer.sex},#{roomer.idNumber},#{roomer.isVip})")
    public int save(@Param("roomer") Map<String,String> map);

    @Update("update t_roomer_info " +
            " set roomer_name=#{roomer.name},roomer_tel=#{roomer.tel},roomer_sex=#{roomer.sex},roomer_idnumber=#{roomer.idNumber},roomer_isvip=#{roomer.isVip}" +
            " where id=#{roomer.id}")
    public int update(@Param("roomer") Map<String,String> map);

    @Delete("delete from t_roomer_info where id=#{id}")
    public int delete(int id);

    @Select("select * from t_roomer_info where id=#{id}")
    public Map<String,Object> findById(int id);

//    在入住登记时可以通过身份证查找到该住户
    @Select("select * from t_roomer_info where roomer_idnumber=#{roomer.idNumber}")
    public Map<String,Object> findByIdCard(@Param("roomer") Map<String,String> map);

    @Select("select * from t_roomer_info")
    public List<Map<String,Object>> findAll();

}
