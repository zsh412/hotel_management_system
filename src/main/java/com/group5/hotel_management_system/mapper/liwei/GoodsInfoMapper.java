package com.group5.hotel_management_system.mapper.liwei;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface GoodsInfoMapper {
    @Insert("insert into t_goodsstatus_info(goods_name,goods_price,goods_number,goods_type)" +
            "value(#{goods.name},#{goods.price},#{goods.number},#{goods.type})")
    public int save(@Param("goods") Map<String, String> map);

    @Update("update t_goodsstatus_info " +
            " set goods_name=#{goods.name},goods_price=#{goods.price},goods_number=#{goods.number},goods_type=#{goods.type}" +
            " where id=#{goods.id}")
    public int update(@Param("goods") Map<String, String> goods);

    @Delete("delete from t_goodsstatus_info where id=#{id}")
    public int delete(int id);

    @Select("select * from t_goodsstatus_info where id=#{id}")
    public Map<String, Object> findById(int id);

    @Select("select * from t_goodsstatus_info where goods_name=#{name}")
    public Map<String, Object> findByName(String name);

    @Select("select * from t_goodsstatus_info")
    public List<Map<String, Object>> findAll();
}
