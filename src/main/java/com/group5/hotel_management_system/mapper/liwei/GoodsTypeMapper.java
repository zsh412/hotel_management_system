package com.group5.hotel_management_system.mapper.liwei;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface GoodsTypeMapper {
    @Insert("insert into t_goodstype_info(goodsType_name)" +
            "value(#{goodsType.name})")
    public int save(@Param("goodsType") Map<String, String> map);

    @Update("update t_goodstype_info " +
            " set goodsType_name=#{goodsType.name}" +
            " where id=#{goodsType.id}")
    public int updateGoodsType(@Param("goodsType") Map<String, String> map);

    @Delete("delete from t_goodstype_info where id=#{id}")
    public int deleteGoodsType(int id);

    @Select("select * from t_goodstype_info where id=#{id}")
    public Map<String, Object> findByIdGoodsType(int id);

    @Select("select * from t_goodstype_info where goodsType_name=#{name}")
    public Map<String, Object> findByNameGoodsType(String name);

    @Select("select * from t_goodstype_info")
    public List<Map<String, Object>> findAllGoodsType();
}