package com.group5.hotel_management_system.mapper.haoran;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper

public interface BuyInfoMapper {

    @Update("CREATE TABLE  IF NOT EXISTS  ${tableBuyName}  (\n" +
            "  `id` int(0) NOT NULL AUTO_INCREMENT,\n" +
            "  `buy_goods` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '购买的商品名称',\n" +
            "  `goods_price` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '单价',\n" +
            "  `buy_num` int(0) NULL DEFAULT NULL COMMENT '购买数量',\n" +
            "  PRIMARY KEY (`id`) USING BTREE\n" +
            ") ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;")
    public  int create(String tableBuyName);

    @Select("select * from ${buy.tableBuyName}")
    public List<Map<String,Object>> findAll(@Param("buy") Map<String,String> map);

    @Select("select * from ${buy.tableBuyName} where id=#{id}")
    public Map<String,Object> findById(@Param("buy") Map<String,String> map);




    @Select("select * from ${buy.tableBuyName} where buy_goods=#{buy.goods}")
    public Map<String,Object> findByName(@Param("buy") Map<String,String> map);

    @Delete("delete from ${buy.tableBuyName} where id=#{id}")
    public int delete(@Param("buy") Map<String,String> map);

    @Update("update ${buy.tableBuyName} " +
            " set buy_goods=#{buy.goods},goods_price=#{buy.price},buy_num=#{buy.num}" +
            " where id=#{buy.id}")
    public int update(@Param("buy") Map<String,String> map);

    @Insert("insert into ${buy.tableBuyName}(buy_goods,goods_price,buy_num)"
            +"value(#{buy.goods},#{buy.price},#{buy.num})")
    public int save(@Param("buy") Map<String,String> map);

    //清空表
    @Update("truncate table ${buy.tableBuyName}")
    public void clear(@Param("buy") Map<String,String> map);


}
