package com.group5.hotel_management_system.mapper.zhujiong;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoomListMappper {

    @Select("select * from t_room_info")
    List<Map<String,Object>> findAllRoom();

    @Update("update t_room_info " +
            "set room_num=#{r.num},room_type=#{r.type},room_floor=#{r.floor}"+
            "where id = #{r.id}")
    public int update(@Param("r") Map<String,String> room);

    @Delete("delete from t_room_info where id=#{id}")
    public int delete(int room_id);

    @Insert("insert into t_room_info(room_num,room_type,room_floor)" +
            "value(#{room.num},#{room.type},#{room.floor})")
    public int save(@Param("room") Map<String,String> map);

    @Update("update t_room_info " +
            "set room_type=#{type.new}" +
            "where room_type = #{type.old}")
    public int updateType(@Param("type") Map<String,String> type);

}
