package com.group5.hotel_management_system.mapper.zhujiong;

import org.apache.ibatis.annotations.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Mapper
public interface FloorInfoMapper {

    @Select("select * from t_floor_info")
    List<Map<String,Object>> findAll();

    @Update("update t_floor_info " +
            "set floor_name=#{f.name},start_room=#{f.start_name},end_room=#{f.end_name},building=#{f.building}"+
            "where id = #{f.id}")
    public int update(@Param("f") Map<String,String> floor);

    @Delete("delete from t_floor_info where id=#{id}")
    public int delete(int id);

    @Insert("insert into t_floor_info(floor_name,start_room,end_room,building)" +
            "value(#{floor.name},#{floor.start},#{floor.end},#{floor.building})")
    public int saveFloor(@Param("floor") Map<String,String> map);
}
