package com.group5.hotel_management_system.mapper.zhujiong;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoomTypeInfoMapper {
    @Select("select * from t_roomtype_info")
    List<Map<String,Object>> findAllRoomType();

    @Update("update t_roomtype_info " +
            "set type=#{t.type},type_price=#{t.price}"+
            "where id = #{t.id}")
    public int updateRoomType(@Param("t") Map<String,String> type);

    @Delete("delete from t_roomtype_info where id=#{id}")
    public int deleteRoomType(int id);

    @Insert("insert into t_roomtype_info(type,type_price)" +
            "value(#{type.type},#{type.type_price})")
    public int saveRoomType(@Param("type") Map<String,String> map);

    //按类型查找
    @Select("select * from t_roomtype_info where type=#{type.type}")
    public Map<String,Object> findByType(@Param("type") Map<String,String> map);
}
