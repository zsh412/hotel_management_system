package com.group5.hotel_management_system.mapper.zhujiong;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface LogInfoMapper {
    @Insert("insert into t_log_info(exe_time,exe_person)" +
            "value(#{log.time},#{log.person})")
    public int saveLog(@Param("log") Map<String,String> map);


    @Select("select * from t_log_info")
    List<Map<String,Object>> findAll();
}
