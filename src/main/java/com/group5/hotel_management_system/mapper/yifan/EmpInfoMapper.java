package com.group5.hotel_management_system.mapper.yifan;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;
@Mapper
public interface EmpInfoMapper {

    @Insert("insert into t_emp_info(emp_name,emp_sex,emp_tel,emp_stuff,emp_level,emp_pin,emp_idCard)" +
            "value(#{emp.name},#{emp.sex},#{emp.tel},#{emp.stuff},#{emp.level},#{emp.pin},#{emp.idCard})")
    public int save(@Param("emp") Map<String,String> map);

    @Update("update t_emp_info " +
            " set emp_name=#{emp.name},emp_tel=#{emp.tel},emp_stuff=#{emp.stuff},emp_level=#{emp.level},emp_pin=#{emp.pin}" +
            " where id=#{emp.id}")
    public int update(@Param("emp") Map<String,String> map);

    @Delete("delete from t_emp_info where id=#{id}")
    public int delete(int id);

    @Select("select * from t_emp_info where id=#{id}")
    public Map<String,Object> findById(int id);

    @Select("select * from t_emp_info where emp_name=#{name}")
    public Map<String,Object> findByName(String name);

    @Select("select * from t_emp_info where emp_idCard=#{idCard}")
    public Map<String,Object> findByIdCard(String idCard);

    @Select("select * from t_emp_info")
    public List<Map<String,Object>> findAll();

}
