package com.group5.hotel_management_system.mapper.yifan;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;
@Mapper
public interface VipInfoMapper {
    @Insert("insert into t_vip_info(vip_name,vip_sex,vip_starttime,vip_tel,vip_idCard)" +
            "value(#{vip.name},#{vip.sex},#{vip.starttime},#{vip.tel},#{vip.idCard})")
    public int save(@Param("vip") Map<String,String> map);

    @Update("update t_vip_info " +
            " set vip_name=#{vip.name},vip_tel=#{vip.tel}" +
            " where id=#{vip.id}")
    public int update(@Param("vip") Map<String,String> map);

    @Delete("delete from t_vip_info where id=#{id}")
    public int delete(int id);

    @Select("select * from t_vip_info where id=#{id}")
    public Map<String,Object> findById(int id);

    @Select("select * from t_vip_info where vip_name=#{name}")
    public Map<String,Object> findByName(String name);

    @Select("select * from t_vip_info")
    public List<Map<String,Object>> findAll();

    @Select("select * from t_vip_info where vip_idCard=#{idCard}")
    public Map<String,Object> findByIdCard(String idCard);
}
