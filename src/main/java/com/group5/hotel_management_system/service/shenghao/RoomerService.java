package com.group5.hotel_management_system.service.shenghao;

import com.group5.hotel_management_system.mapper.shenghao.RoomerInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class RoomerService {
    @Resource
    RoomerInfoMapper roomerInfoMapper;

    public boolean doSave(Map<String,String> map){
        boolean flag=false;
        //ctrl+alt+t 选择surround with窗口 选择try-catch
        try {
            int r=roomerInfoMapper.save(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doDelete(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=roomerInfoMapper.delete(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doUpdate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=roomerInfoMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }



    public Map<String,Object> findById(Map<String,String> map){
        Map<String,Object> roomer=null;
        try {
            int id=Integer.parseInt(map.get("id"));
            roomer=roomerInfoMapper.findById(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return roomer;
    }

    public Map<String,Object> findByIdCard(Map<String,String> map){
        Map<String,Object> roomer=null;
        try {
            roomer=roomerInfoMapper.findByIdCard(map);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return roomer;
    }

    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=null;
        try {
            list=roomerInfoMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
