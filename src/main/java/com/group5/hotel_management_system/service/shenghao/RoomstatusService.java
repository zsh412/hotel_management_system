package com.group5.hotel_management_system.service.shenghao;

import com.group5.hotel_management_system.mapper.shenghao.RoomInfoMapper;
import com.group5.hotel_management_system.mapper.shenghao.RoomstatusInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class RoomstatusService {
    @Resource
    RoomstatusInfoMapper roomstatusInfoMapper;

    public boolean doSave(Map<String,String> map){
        boolean flag=false;
        //ctrl+alt+t 选择surround with窗口 选择try-catch
        try {
            int r=roomstatusInfoMapper.save(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doCreate(String tableName){
        boolean flag=false;
        //ctrl+alt+t 选择surround with窗口 选择try-catch
        try {
            int r=roomstatusInfoMapper.create(tableName);
            if(r>=0){
                flag=true;
            }
            else
                System.out.println(r);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doDelete(Map<String,String> map){
        boolean flag=false;
        try {

            int r=roomstatusInfoMapper.delete(map);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doUpdate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=roomstatusInfoMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doUpdateByDate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=roomstatusInfoMapper.updateByDate(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public Map<String,Object> findById(Map<String,String> map){
        Map<String,Object> room=null;
        try {
            room=roomstatusInfoMapper.findById(map);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return room;
    }

    public Map<String,Object> findByDate(Map<String,String> map){
        Map<String,Object> room=null;
        try {
            room=roomstatusInfoMapper.findByDate(map);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return room;
    }

    public List<Map<String,Object>> findAll(Map<String,String> map){
        List<Map<String,Object>> list=null;
        try {
            list=roomstatusInfoMapper.findAll(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
