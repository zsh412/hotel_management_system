package com.group5.hotel_management_system.service.shenghao;

import com.group5.hotel_management_system.mapper.shenghao.RoomInfoMapper;
import com.group5.hotel_management_system.mapper.shenghao.RoomerInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class RoomService {
    @Resource
    RoomInfoMapper roomInfoMapper;

    public boolean doSave(Map<String,String> map){
        boolean flag=false;
        //ctrl+alt+t 选择surround with窗口 选择try-catch
        try {
            int r=roomInfoMapper.save(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doDelete(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=roomInfoMapper.delete(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doUpdate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=roomInfoMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public Map<String,Object> findById(Map<String,String> map){
        Map<String,Object> room=null;
        try {
            int id=Integer.parseInt(map.get("id"));
            room=roomInfoMapper.findById(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return room;
    }

    public Map<String,Object> findByNum(Map<String,String> map){
        Map<String,Object> room=null;
        try {
            String num=map.get("num");
            room=roomInfoMapper.findByNum(num);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return room;
    }

    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=null;
        try {
            list=roomInfoMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
