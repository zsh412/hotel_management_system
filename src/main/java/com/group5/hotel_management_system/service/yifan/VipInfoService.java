package com.group5.hotel_management_system.service.yifan;
import com.group5.hotel_management_system.mapper.yifan.VipInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service
public class VipInfoService {
    @Resource
    private VipInfoMapper vipInfoMapper;
    public boolean doSave(Map<String,String> map){
        boolean flag=false;
        try {
            int r=vipInfoMapper.save(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    public boolean doDelete(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=vipInfoMapper.delete(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }
    public boolean doUpdate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=vipInfoMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=null;
        try {
            list=vipInfoMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Map<String,Object> findById(Map<String,String> map){
        Map<String,Object> vip=null;
        try {
            int id=Integer.parseInt(map.get("id"));
            vip=vipInfoMapper.findById(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return vip;
    }
    public Map<String,Object> findByName(Map<String,String> map) {
        Map<String, Object> vip = null;
        try {
            String name = map.get("name");
            vip = vipInfoMapper.findByName(name);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return vip;
    }
    public Map<String,Object> findByIdCard(Map<String,String> map) {
        Map<String, Object> vip = null;
        try {
            String idCard = map.get("idCard");
            vip = vipInfoMapper.findByIdCard(idCard);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return vip;
    }

}
