package com.group5.hotel_management_system.service.yifan;
import com.group5.hotel_management_system.mapper.yifan.EmpInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service
public class EmpInfoService {
    @Resource
    private EmpInfoMapper empInfoMapper;
    public boolean doSave(Map<String,String> map){
        boolean flag=false;
        //选择要添加监控的代码
        //ctrl+alt+t 打开 surround with窗口 选择 try catch
        try {
            int r=empInfoMapper.save(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            //记录日志，写到文件中进行保存
        }
        return flag;
    }
    public boolean doDelete(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=empInfoMapper.delete(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }
    public boolean doUpdate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=empInfoMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=null;
        try {
            list=empInfoMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    public Map<String,Object> findById(Map<String,String> map){
        Map<String,Object> emp=null;
        try {
            int id=Integer.parseInt(map.get("id"));
            emp=empInfoMapper.findById(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return emp;
    }
    public Map<String,Object> findByName(Map<String,String> map){
        Map<String,Object> res=null;
        try {
            String name=map.get("name");
            res=empInfoMapper.findByName(name);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return res;
    }
    public Map<String,Object> findByIdCard(Map<String,String> map) {
        Map<String, Object> vip = null;
        try {
            String idCard = map.get("idCard");
            vip = empInfoMapper.findByIdCard(idCard);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return vip;
    }
}
