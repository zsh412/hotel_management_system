package com.group5.hotel_management_system.service.liwei;

import com.group5.hotel_management_system.mapper.liwei.GoodsTypeMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class GoodsTypeService {
    @Resource
    private GoodsTypeMapper goodsTypeMapper;

    public boolean doSaveGoodsType(Map<String,String> map){
        boolean flag=false;
        //选择要添加监控的代码
        //ctrl+alt+t 打开 surround with窗口 选择 try catch
        try {
            int r=goodsTypeMapper.save(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            //记录日志，写到文件中进行保存
        }
        return flag;
    }
    public boolean doDeleteGoodsType(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=goodsTypeMapper.deleteGoodsType(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }
    public boolean doUpdateGoodsType(Map<String,String> map){
        boolean flag=false;
        try {
            int r=goodsTypeMapper.updateGoodsType(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    public List<Map<String,Object>> findAllGoodsType(){
        List<Map<String,Object>> list=null;
        try {
            list=goodsTypeMapper.findAllGoodsType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    public Map<String,Object> findByIdGoodsType(Map<String,String> map){
        Map<String,Object> goodsType = null;
        try {
            int id=Integer.parseInt(map.get("id"));
            goodsType = goodsTypeMapper.findByIdGoodsType(id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return goodsType;
    }
    public Map<String,Object> findByNameGoodsType(Map<String,String> map){
        Map<String,Object> goodsType = null;
        try {
            String name=map.get("name");
            goodsType = goodsTypeMapper.findByNameGoodsType(name);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return goodsType;
    }
}