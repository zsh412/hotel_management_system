package com.group5.hotel_management_system.service.zhujiong;

import com.group5.hotel_management_system.mapper.zhujiong.RoomListMappper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class RoomListService {
    @Resource
    private RoomListMappper roomListMapper;

    public List<Map<String,Object>> findAllRoom(){
        List<Map<String,Object>> list=null;
        try {
            list=roomListMapper.findAllRoom();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public boolean deleteRoom(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=roomListMapper.delete(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean updateRoom(Map<String,String> map){
        boolean flag=false;
        try {
            int r=roomListMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean saveRoom(Map<String,String> map){
        boolean flag = false;
        try {
            int r = roomListMapper.save(map);
            if(r>0){
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean updateType(Map<String,String> map){
        boolean flag=false;
        try {
            int r=roomListMapper.updateType(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
}
