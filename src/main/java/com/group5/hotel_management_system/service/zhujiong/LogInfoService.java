package com.group5.hotel_management_system.service.zhujiong;

import com.group5.hotel_management_system.mapper.zhujiong.LogInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class LogInfoService {
    @Resource
    private LogInfoMapper logInfoMapper;

    public boolean saveLog(Map<String,String> map){
        boolean flag = false;
        try {
            int f = logInfoMapper.saveLog(map);
            if(f>0){
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=null;
        try {
            list=logInfoMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
