package com.group5.hotel_management_system.service.zhujiong;

import com.group5.hotel_management_system.mapper.zhujiong.RoomTypeInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class RoomTypeInfoService {
    @Resource
    private RoomTypeInfoMapper roomTypeInfoMapper;

    public List<Map<String,Object>> findAllRoomType(){
        List<Map<String,Object>> list=null;
        try {
            list=roomTypeInfoMapper.findAllRoomType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public boolean doUpdateRoomType(Map<String,String> map){
        boolean flag=false;
        try {
            int r=roomTypeInfoMapper.updateRoomType(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doDeleteRoomType(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=roomTypeInfoMapper.deleteRoomType(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean saveRoomType(Map<String,String> map){
        boolean flag = false;
        try {
            int f = roomTypeInfoMapper.saveRoomType(map);
            if(f>0){
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public Map<String,Object> findByType(Map<String,String> map){
        Map<String,Object> type=null;
        try {
            type=roomTypeInfoMapper.findByType(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return type;
    }
}
