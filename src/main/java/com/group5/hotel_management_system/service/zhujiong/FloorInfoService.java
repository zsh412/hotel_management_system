package com.group5.hotel_management_system.service.zhujiong;

import com.group5.hotel_management_system.mapper.zhujiong.FloorInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class FloorInfoService {

    @Resource
    private FloorInfoMapper floorInfoMapper;

    public List<Map<String,Object>> findAll(){
        List<Map<String,Object>> list=null;
        try {
            list=floorInfoMapper.findAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public boolean doUpdate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=floorInfoMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doDelete(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=floorInfoMapper.delete(id);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean saveFloor(Map<String,String> map){
        boolean flag = false;
        try {
            int f = floorInfoMapper.saveFloor(map);
            if(f>0){
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
}
