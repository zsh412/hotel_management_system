package com.group5.hotel_management_system.service.haoran;

import com.group5.hotel_management_system.mapper.haoran.BuyInfoMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class BuyInfoService {
    @Resource
    BuyInfoMapper buyInfoMapper;
    public boolean doSave(Map<String,String> map){
        boolean flag=false;
        //ctrl+alt+t 选择surround with窗口 选择try-catch
        try {
            int r=buyInfoMapper.save(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    public boolean doCreate(String tableName){
        boolean flag=false;
        //ctrl+alt+t 选择surround with窗口 选择try-catch
        try {
            int r=buyInfoMapper.create(tableName);
            if(r>=0){
                flag=true;
            }
            else
                System.out.println(r);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
    public boolean doDelete(Map<String,String> map){
        boolean flag=false;
        try {
            int id=Integer.parseInt(map.get("id"));
            int r=buyInfoMapper.delete(map);
            if(r>0){
                flag=true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return flag;
    }

    public boolean doUpdate(Map<String,String> map){
        boolean flag=false;
        try {
            int r=buyInfoMapper.update(map);
            if(r>0){
                flag=true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public Map<String,Object> findById(Map<String,String> map){
        Map<String,Object> buyInfo=null;
        try {
            int id=Integer.parseInt(map.get("id"));
            buyInfo=buyInfoMapper.findById(map);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return buyInfo;
    }

    public List<Map<String,Object>> findAll(Map<String,String> map){
        List<Map<String,Object>> list=null;
        try {
            list=buyInfoMapper.findAll(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Map<String,Object> findByName(Map<String,String> map){
        Map<String,Object> goods=null;
        try {
            goods=buyInfoMapper.findByName(map);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return goods;
    }

    public void clear(Map<String,String> map){
        buyInfoMapper.clear(map);
    }

}
