package com.group5.hotel_management_system.controller.haoran;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;

import java.nio.charset.Charset;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BuyInfoControllerTest {

    /**
     * 模拟客户端浏览器像服务器发送结果
     */
    @Resource
    private MockMvc mockMvc;

    @Test
    public void findAll() throws Exception {
        MockHttpServletRequestBuilder builder=
                MockMvcRequestBuilders.post("/buy_all")
                        .param("tableBuyName","t_A202buy_info");
        MvcResult rst=mockMvc.perform(builder).andReturn();
        String r= rst.getResponse().getContentAsString(Charset.forName("UTF-8"));

        System.out.println(r);
    }
}