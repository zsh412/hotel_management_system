package com.group5.hotel_management_system.controller.shenghao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;

import java.nio.charset.Charset;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RoomstatusControllerTest {

    /**
     * 模拟客户端浏览器像服务器发送结果
     */
    @Resource
    private MockMvc mockMvc;

    @Test
    public void findByDate() throws Exception {
        MockHttpServletRequestBuilder builder=
                MockMvcRequestBuilders.post("/roomstatus_findbydate")
                        .param("tableName","t_A101_info")
                        .param("time_in","2020-08-29");
        MvcResult rst=mockMvc.perform(builder).andReturn();
         String r= rst.getResponse().getContentAsString(Charset.forName("UTF-8"));

        System.out.println(r);
    }

}