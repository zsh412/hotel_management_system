package com.group5.hotel_management_system.mapper.zhujiong;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RoomTypeInfoMapperTest {

    @Resource
    private RoomTypeInfoMapper roomTypeInfoMapper;
    @Test
    public void findAllRoomType() {
        List<Map<String,Object>> list=roomTypeInfoMapper.findAllRoomType();
        System.out.print(list);
    }

    @Test
    public void updateRoomType() {
        Map<String,String> map=new HashMap<>();
        map.put("type","wgy和爸爸我的豪华海景房");
        map.put("price","111");
        map.put("id","1");
        int n=roomTypeInfoMapper.updateRoomType(map);
        assertEquals(1,n);
    }

    @Test
    public void deleteRoomType() {
        int n=roomTypeInfoMapper.deleteRoomType(1);
        assertEquals(1,n);
    }

    @Test
    public void saveRoomType() {
        Map<String,String> map=new HashMap<>();
        map.put("type","普通单人房");
        map.put("type_price","150");
        //map.put("id","1");
        int n=roomTypeInfoMapper.saveRoomType(map);
        assertEquals(1,n);
    }
}