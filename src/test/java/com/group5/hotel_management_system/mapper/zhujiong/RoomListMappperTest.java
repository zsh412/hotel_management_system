package com.group5.hotel_management_system.mapper.zhujiong;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RoomListMappperTest {

    @Resource
    private RoomListMappper roomListMappper;
    @Test
    public void findAllRoom() {
        List<Map<String,Object>> list=roomListMappper.findAllRoom();
        System.out.println(list);
    }

    @Test
    public void update() {
        Map<String,String> map=new HashMap<>();
        map.put("num","A201");
        map.put("type","胜豪甜蜜专属");
        map.put("floor","A栋二层");
        map.put("id","2");
        int n=roomListMappper.update(map);
    }

    @Test
    public void delete() {
        roomListMappper.delete(2);
    }

    @Test
    public void updateType() {
        Map<String,String> map=new HashMap<>();
        map.put("old","肥宅双人房");
        map.put("new","普通双人房");
        roomListMappper.updateType(map);
    }
}