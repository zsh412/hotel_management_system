package com.group5.hotel_management_system.mapper.shenghao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RoomerInfoMapperTest {
    @Resource
    RoomerInfoMapper roomerInfoMapper;

    @Test
    public void save() {
        //使用map集合存放数据
        Map<String,String> map=new HashMap<>();
        map.put("name","小赵");
        map.put("tel","17734259984");
        map.put("sex","男");
        map.put("idNumber","130635200010082010");
        map.put("isVip","是");
        int n=roomerInfoMapper.save(map);
//        System.out.println(n);
        //断言检测
        assertEquals(1,n);
    }

    @Test
    public void update() {
        //使用map集合存放数据
        Map<String,String> map=new HashMap<>();
        map.put("name","雅雅");
        map.put("tel","123172");
        map.put("sex","女");
        map.put("idNumber","130635aksnd010");
        map.put("isVip","是");
        map.put("id","5");
        int n=roomerInfoMapper.update(map);
        //断言检测
        assertEquals(1,n);
    }

    @Test
    public void delete() {
        int n=roomerInfoMapper.delete(7);
        assertEquals(1,n);
    }

    @Test
    public void findById() {
        Map<String,Object> map=roomerInfoMapper.findById(3);
        System.out.println(map);
    }

    @Test
    public void findByIdCard() {
        Map<String,String> map=new HashMap<>();
        map.put("idNumber","123456");
        Map<String,Object> roomer=roomerInfoMapper.findByIdCard(map);
        System.out.println(roomer);

    }

    @Test
    public void findAll() {
        List<Map<String,Object>> list=roomerInfoMapper.findAll();
        System.out.println(list);
    }
}