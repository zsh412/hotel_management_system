package com.group5.hotel_management_system.mapper.shenghao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RoomInfoMapperTest {

    @Resource
    RoomInfoMapper roomInfoMapper;
    @Test
    public void save() {
        //使用map集合存放数据
        Map<String,String> map=new HashMap<>();
        map.put("num","A101");
        map.put("type","单人间");
        map.put("floor","A栋");
        int n=roomInfoMapper.save(map);
//        System.out.println(n);
        //断言检测
        assertEquals(1,n);
    }

    @Test
    public void update() {
        //使用map集合存放数据
        Map<String,String> map=new HashMap<>();
        map.put("num","A103");
        map.put("type","单人间");
        map.put("floor","A栋");
        map.put("id","3");
        int n=roomInfoMapper.update(map);
//        System.out.println(n);
        //断言检测
        assertEquals(1,n);
    }

    @Test
    public void delete() {
        int n=roomInfoMapper.delete(4);
        assertEquals(1,n);
    }

    @Test
    public void findById() {
        Map<String,Object> map=roomInfoMapper.findById(3);
        System.out.println(map);
    }

    @Test
    public void findAll() {
        List<Map<String,Object>> list=roomInfoMapper.findAll();
        System.out.println(list);
    }

    @Test
    public void findByNum() {
        Map<String,Object> map=roomInfoMapper.findByNum("A106");
        System.out.println(map);
    }
}