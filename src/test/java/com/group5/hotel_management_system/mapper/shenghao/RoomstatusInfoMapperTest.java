package com.group5.hotel_management_system.mapper.shenghao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RoomstatusInfoMapperTest {

    @Resource
    RoomstatusInfoMapper roomstatusInfoMapper;
    @Test
    public void create() {
       int n= roomstatusInfoMapper.create("t_A101_info");
        roomstatusInfoMapper.create("t_A102_info");
        roomstatusInfoMapper.create("t_A103_info");
        roomstatusInfoMapper.create("t_A104_info");
        roomstatusInfoMapper.create("t_A105_info");
        roomstatusInfoMapper.create("t_A106_info");
        roomstatusInfoMapper.create("t_A201_info");
        roomstatusInfoMapper.create("t_A202_info");
        roomstatusInfoMapper.create("t_A203_info");
        roomstatusInfoMapper.create("t_A204_info");
        roomstatusInfoMapper.create("t_A205_info");
        roomstatusInfoMapper.create("t_A206_info");
        System.out.println(n);
    }

    @Test
    public void save() {
        //使用map集合存放数据
        Map<String,String> map=new HashMap<>();
        map.put("tableName","t_A102_info");
        map.put("roomStatus","已入住");
        map.put("time_in","2020-06-29");
        map.put("deposit","2000");
        System.out.println(roomstatusInfoMapper.save(map));;
    }

    @Test
    public void update() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void findById() {
    }

    @Test
    public void findByDate() {
        //使用map集合存放数据
        Map<String,String> map=new HashMap<>();
        map.put("tableName","t_a101_info");
        map.put("time_in","2020-06-28");
        System.out.println(roomstatusInfoMapper.findByDate(map));;
    }

    @Test
    public void findAll() {
    }

}