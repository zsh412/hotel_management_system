package com.group5.hotel_management_system.mapper.yifan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class EmpInfoMapperTest {
    @Resource
    private EmpInfoMapper empInfoMapper;
    @Test
    public void save() {
        Map<String,String> map=new HashMap<>();
        map.put("name","xdd");
        map.put("sex","男");
        map.put("tel","10010");
        map.put("stuff","1");
        map.put("level","1");
        map.put("pin","654321");
        empInfoMapper.save(map);
    }

    @Test
    public void update() {
        Map<String,String> map=new HashMap<>();
        map.put("name","xdddd");
        map.put("sex","男");
        map.put("tel","10010");
        map.put("stuff","1");
        map.put("level","1");
        map.put("pin","654321");
        map.put("id","2");
        int n=empInfoMapper.update(map);
    }

    @Test
    public void delete() {
        empInfoMapper.delete(1);
    }

    @Test
    public void findById() {
        Map<String,Object> map=empInfoMapper.findById(2);
        System.out.println(map);
    }

    @Test
    public void findAll() {
        List<Map<String,Object>> list=empInfoMapper.findAll();
        System.out.println(list);
    }
}