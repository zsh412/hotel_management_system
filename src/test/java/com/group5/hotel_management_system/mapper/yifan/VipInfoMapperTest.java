package com.group5.hotel_management_system.mapper.yifan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class VipInfoMapperTest {
    @Resource
    private VipInfoMapper vipInfoMapper;
    @Test
    public void save() {
        Map<String,String> map=new HashMap<>();
        map.put("name","xdasdd2131");
        map.put("sex","男");
        map.put("starttime","1256adfad1");
        map.put("tel","1230010");
        map.put("idCard","1233123151");
        vipInfoMapper.save(map);
    }

    @Test
    public void update() {
        Map<String,String> map=new HashMap<>();
        map.put("name","xddaad");
        map.put("sex","男");
        map.put("tel","10010");
        map.put("starttime","123531561");
        map.put("idCard","1123123315132451");
        map.put("id","1");
        int n=vipInfoMapper.update(map);
    }

    @Test
    public void delete() {

        vipInfoMapper.delete(3);
    }

    @Test
    public void findById() {
        Map<String,Object> map=vipInfoMapper.findById(3);
        System.out.println(map);
    }

    @Test
    public void findAll() {
        List<Map<String,Object>> list=vipInfoMapper.findAll();
        System.out.println(list);
    }


}