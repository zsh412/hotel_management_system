package com.group5.hotel_management_system.mapper.liwei;

import com.group5.hotel_management_system.mapper.yifan.EmpInfoMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GoodsInfoMapperTest {
    @Resource
    private GoodsInfoMapper goodsInfoMapper;
    @Test
    public void save() {
        //使用map集合存放数据
        Map<String,String> map=new HashMap<>();
        map.put("name","矿泉水");
        map.put("price","2");
        map.put("number","100");
        map.put("type","饮食");
        int n=goodsInfoMapper.save(map);
        //断言检测
        assertEquals(1,n);
    }

    @Test
    public void update() {
        Map<String,String> map=new HashMap<>();
        map.put("name","百岁山");
        map.put("price","3");
        map.put("number","100");
        map.put("type","饮食");
        map.put("id","1");
        int n=goodsInfoMapper.update(map);
    }

    @Test
    public void delete() {
        goodsInfoMapper.delete(1);
    }

    @Test
    public void findById() {
        Map<String,Object> map=goodsInfoMapper.findById(2);
        System.out.println(map);
    }

    @Test
    public void findByName() {
        Map<String,Object> map=goodsInfoMapper.findByName("百岁山");
        System.out.println(map);
    }

    @Test
    public void findAll() {
        List<Map<String,Object>> list=goodsInfoMapper.findAll();
        System.out.println(list);
    }
}