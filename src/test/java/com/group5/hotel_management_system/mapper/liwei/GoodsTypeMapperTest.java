package com.group5.hotel_management_system.mapper.liwei;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GoodsTypeMapperTest {
    @Resource
    private GoodsTypeMapper goodsTypeMapper;
    @Test
    public void save() {
        //使用map集合存放数据
        Map<String,String> map = new HashMap<>();
        map.put("name","饮食");
        int n=goodsTypeMapper.save(map);
        //断言检测
        assertEquals(1,n);
    }

    @Test
    public void update() {
        Map<String,String> map=new HashMap<>();
        map.put("type","饮食");
        map.put("id","1");
        int n=goodsTypeMapper.updateGoodsType(map);
    }

    @Test
    public void delete() {
        goodsTypeMapper.deleteGoodsType(1);
    }

    @Test
    public void findById() {
        Map<String,Object> map=goodsTypeMapper.findByIdGoodsType(2);
        System.out.println(map);
    }

    @Test
    public void findByName() {
        Map<String,Object> map=goodsTypeMapper.findByNameGoodsType("小王八");
        System.out.println(map);
    }

    @Test
    public void findAll() {
        List<Map<String,Object>> list=goodsTypeMapper.findAllGoodsType();
        System.out.println(list);
    }
}